import Home from "@/components/Home/index.vue"
import { mount, flushPromises } from "@vue/test-utils"
import * as mocks from "@/components/Home/Timeline/Post/mocks"

jest.mock("axios", () => ({
  get: (url: string) => ({
    data: [mocks.todayPost, mocks.thisWeekPost, mocks.thisMonthPost],
  }),
}))

describe("Home", () => {
  it("render preload", async () => {
    const wrapper = mount(Home)
    expect(wrapper.find(".loader").exists()).toBe(true)
  })
  it("renders period 3 times", async () => {
    const wrapper = mount(Home)
    await flushPromises()
    expect(wrapper.findAll('[role="tab"]')).toHaveLength(3)
  })

  it("updates the period when clicked", async () => {
    const wrapper = mount(Home)

    await flushPromises()

    const $today = wrapper.findAll('[role="tab"]')[0]
    expect($today.attributes()).toHaveProperty("aria-selected", "true")

    const $thisWeek = wrapper.findAll('[role="tab"]')[1]
    await $thisWeek.trigger("click")
    expect($today.attributes()).toHaveProperty("aria-selected", "false")
    expect($thisWeek.attributes()).toHaveProperty("aria-selected", "true")

    const $thisMonth = wrapper.findAll('[role="tab"]')[2]
    await $thisMonth.trigger("click")
    expect($today.attributes()).toHaveProperty("aria-selected", "false")
    expect($thisWeek.attributes()).toHaveProperty("aria-selected", "false")
    expect($thisMonth.attributes()).toHaveProperty("aria-selected", "true")
  })

  it("renders todays post by default", async () => {
    const wrapper = mount(Home)

    await flushPromises()

    const $todayPosts = wrapper.find('[role="tabpanel"]')
    expect($todayPosts.findAll('[data-test="post"]')).toHaveLength(1)
  })

  it("render posts per period", async () => {
    const wrapper = mount(Home)

    await flushPromises()

    const $thisWeek = wrapper.findAll('[role="tab"]')[1]
    await $thisWeek.trigger("click")
    const $thisWeekPosts = wrapper.findAll('[role="tabpanel"]')[1]
    expect($thisWeekPosts.findAll('[data-test="post"]')).toHaveLength(2)

    const $thisMonth = wrapper.findAll('[role="tab"]')[2]
    await $thisMonth.trigger("click")
    const $thisMonthPosts = wrapper.findAll('[role="tabpanel"]')[2]
    expect($thisMonthPosts.findAll('[data-test="post"]')).toHaveLength(3)
  })
})
