import { InjectionKey } from "vue"
import { createStore, useStore as baseUseStore, Store } from "vuex"
import { Post } from "@/types"
import axios from "axios"

// initial state
export const initialState = (): State => ({
  posts: initialPostsState(),
})

// posts
export interface PostsState {
  ids: string[]
  all: Record<string, Post>
  loaded: boolean
}

const initialPostsState = (): PostsState => ({
  ids: [],
  all: {},
  loaded: false,
})

export interface State {
  posts: PostsState
}

export const store = createStore<State>({
  state: initialState(),
  actions: {
    fetchPosts: async ({ commit }) => {
      const { data } = await axios.get<Post[]>("/posts")

      const ids: string[] = []
      const all: Record<string, Post> = {}

      data.forEach((p: Post) => {
        ids.push(p.id.toString())
        all[p.id] = p
      })

      commit("setPosts", { ids, all, loaded: true })
    },
  },
  getters: {
    getPosts: state => state.posts,
  },
  mutations: {
    setPosts: (state, posts: PostsState) => (state.posts = posts),
  },
})

export const key: InjectionKey<Store<State>> = Symbol()

export function useStore() {
  return baseUseStore(key)
}
