import { Post } from "@/types"
import moment from "moment"

export const basePost: Post = {
  id: 1,
  title: "Base post",
  html: "<p>Content</p>",
  markdown: "Content",
  autorId: 1,
  created: moment(),
}

export const todayPost: Post = {
  ...basePost,
  id: 2,
  title: "Today post",
}

export const thisWeekPost: Post = {
  ...basePost,
  id: 3,
  title: "This week post",
  created: moment().subtract(2, "day"),
}

export const thisMonthPost: Post = {
  ...basePost,
  id: 4,
  title: "This month post",
  created: moment().subtract(2, "week"),
}
