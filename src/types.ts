import { Moment } from "moment"

export type Period = "today" | "this week" | "this month"
export interface Post {
  id: number
  title: string
  html: string
  markdown: string
  autorId: number
  created: Moment
}
