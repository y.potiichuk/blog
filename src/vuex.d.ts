import { ComponentCustomProperties } from "vue"
import { Store } from "vuex"
import { PostsState } from "@/store"

declare module "@vue/runtime-core" {
  interface State {
    posts: PostsState
  }

  interface ComponentCustomProperties {
    $store: Store<State>
  }
}
