import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router"
import Home from "@/components/Home/index.vue"

const HomeRoute: RouteRecordRaw = {
  path: "/",
  name: "home",
  component: Home,
}
const CreatePostRoute: RouteRecordRaw = {
  path: "/create-post",
  name: "create-post",
  component: () => import("@/components/CreatePost/index.vue"),
}
const routes: Array<RouteRecordRaw> = [HomeRoute, CreatePostRoute]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
