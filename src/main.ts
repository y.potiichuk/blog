import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import { store, key } from "./store"
import axios from "axios"

// styles
import PrimeVue from "primevue/config"
import "@/assets/styles/index.scss"
import "primevue/resources/themes/mdc-light-deeppurple/theme.css"
import "primeicons/primeicons.css"
import "primevue/resources/primevue.min.css"

import * as mocks from "@/components/Home/Timeline/Post/mocks"

const delay = (ms: number) => new Promise(res => setTimeout(res, ms))

// @ts-ignore
axios.get = async (url: string) => {
  await delay(2000)
  if (url === "/posts") {
    return {
      data: [mocks.todayPost, mocks.thisWeekPost, mocks.thisMonthPost],
    }
  }
}

createApp(App).use(store, key).use(router).use(PrimeVue).mount("#app")
